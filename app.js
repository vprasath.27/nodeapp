try {
  var express = require('express');
  var http = require('http');
  var app = express();
  app.get('/test', function (req, res) {
  // app.post('/test', function (req, res) {
  // app.use('/test', function (req, res) {
    console.log(req.path, 'Route name');
    // res.send('Success')
  });

  // middleware to catch not existing routes
  app.use(notexistingroutes);

  function notexistingroutes(req, res, next) {
    var error = new Error(req.apth + ' Requested route is not available')
    console.log(error);
    next(error)
  }
  app.use(erroredRoutes);
  function erroredRoutes(err, req, res, next) {
    err.status = 404
    res.send(err)
  }

  var port = 3002;
  // app.listen(port);
  var server = app.listen(port)
  // var server = http.createServer(app)
    // .listen(port);
    console.log(server.timeout,"server.timeout")
  // server.timeout = 60000
  console.log(`server listening in ${port}`)
} catch (error) {
  console.log(error, 'Catch Error***************');
}